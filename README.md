# BackEnd Test Code

- Laravel 10
- Database Mysql

How to Start
1. `cp .env.example .env`
2. `composer update`
3. `php artisan migrate:fresh --seed`

- All users using same password ('password')
- Default migration except users migration deleted
- Default Prefix /api routes removed, following instruction which not used /api prefix

## Route List

```
POST       auth/login ..................................................................... login › API\AuthController@login
POST       auth/signup .................................................................. signup ›
GET|HEAD   user/userlist ............................................................. user.index › API\UserController@index
```
