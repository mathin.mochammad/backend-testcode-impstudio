<?php

namespace App\Rules;

use App\Models\User;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class CustomUniqueUsernameRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $exists = User::whereUsername($value)->exists();

        if ($exists) {
            throw new ConflictHttpException('User already exists');
        }
    }
}
