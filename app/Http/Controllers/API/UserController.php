<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\API\UserCollection;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $pageSize = request()->get('limit') ?? 10;

        $users = User::orderBy('created_at', 'desc')->simplePaginate($pageSize);

        return response()->json($users);
    }
}
