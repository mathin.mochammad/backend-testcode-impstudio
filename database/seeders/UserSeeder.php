<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = [
            [
                'username' => 'admin',
                'fullname' => 'Super Administrator',
                'password' => 'password',
            ],
            [
                'username' => 'mathin',
                'fullname' => 'Mathin Mochammad',
                'password' => 'password'
            ]
        ];

        foreach ($users as $key => $user) {
            User::updateOrCreate([
                'username' => $user['username']
            ], [
                'fullname' => $user['fullname'],
                'password' => bcrypt($user['password']),
            ]);
        }

        # Generate More 20 Random Dummy users
        \App\Models\User::factory(20)->create();
    }
}
